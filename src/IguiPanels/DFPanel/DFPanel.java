package IguiPanels.DFPanel;

import ipc.Partition;
import is.AnyInfo;
import is.InfoEvent;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JToolTip;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.event.RendererChangeEvent;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import Igui.Igui;
import Igui.IguiConstants.AccessControlStatus;
import Igui.IguiConstants.MessageSeverity;
import Igui.IguiException.UnknownTransition;
import Igui.IguiLogger;
import Igui.IguiPanel;
import Igui.RunControlFSM;
import Igui.RunControlFSM.State;
import Igui.RunControlFSM.Transition;
import Igui.Common.MultiLineToolTip;
import IguiPanels.DFPanel.DFPanelExceptions.ISException;
import config.ConfigException;

import com.jidesoft.swing.SimpleScrollPane;

import dal.IS_EventsAndRates;


/**
 * This panel contains plots to monitor IS variables defined in the partition object "IS_InformationSource".
 * <p>
 * The panel is made up of two tabs: the first one contains a plot of rates for LVL1, LVL1, EB, EF and Recording rates; the second one will
 * show all the rated defined in the 'Others' attribute of the "IS_InformationSources" object (please, consider that in this case only the
 * 'Rate' counter will be reported; the 'EventCounter' will be shown as a label next to the 'Description' - used to identify the different
 * objects to monitor).
 */
public class DFPanel extends IguiPanel {
    private static final long serialVersionUID = -728491567176508122L;

    // The following lists are volatile: this helps in making easier the access to the lists
    // from different threads (they are accessed both from the EDT and the IguiPanel own thread).
    // The lists are never modified and whenever a list of receivers has to be modified (i.e., when the
    // database is reloaded) a new one is created and the volatile reference to it is updated.
    // This creates an 'happens before' relationship with the following readings of the list reference
    // and the list accesses are thread safe. For safety the methods creating the fresh lists return an
    // unmodifiable view of the newly created list, so that any misuse can be easily detected.
    // To enforce thread safety the lists contain references to 'RateReceiver' objects, which are immutable
    // (and immutable objects can be safely shared amongst threads without additional synchronization).
    //
    // !!! The volatile reference gives no guaranty about the contents of the lists,
    // that's why a list (and the objects it contains) must not be modified after its creation
    // (without addition synchronization) !!!
    /**
     * List of receivers used to get and process updates from IS for the 'fixed/default' rates to monitor
     */
    private volatile List<RateReceiver> fixedReceivers = Collections.emptyList();

    /**
     * List of receivers used to get and process updates from IS for the 'other' rates to monitor
     */
    private volatile List<RateReceiver> otherReceivers = Collections.emptyList();

    /**
     * The state after which the IS subscriptions have to be executed
     */
    private final static State stateToSubscribe = State.CONNECTED;

    /**
     * The IS subscriptions have to be performed at this transition
     */
    private final static Transition transitionToSubscribe = Transition.START;

    /**
     * The IS un-subscriptions have to be performed at this transition
     */
    private final static Transition transitionToUnsubscribe = Transition.STOPARCHIVING;

    // Labels for check boxes in the 'default' tab
    private final static String L1Label = "L1";
    private final static String HLTLabel = "HLT";
    private final static String RELabel = "RE";

    // Default IS information for rates and event counters (used only if nothing is defined for the standard rates and counters)
    private final static String defServerName = "DF";
    private final static String defInfoName = "HLTSV";
    private final static String defCounterAttribute = "ProcessedEvents";
    private final static String defRateAttribute = "Rate";

    // Default values for spinners
    private final static int defaultDaysHistory = 0;
    private final static int defaultHoursHistory = 2;
    private final static int defaultMinutesHistory = 0;

    /**
     * This panel name
     */
    private final static String pName = "DFPanel";

    /**
     * Reference to the main Igui
     */
    private final Igui mainIgui;

    // Components for the tabs containing the plots:
    // 1 - panels for check boxes
    // 2 - time series collection
    // 3 - spinners for plot history (days, hours and minutes)
    // 4 - ChangeListener listener to change the plot history when a spinner value is changed
    private final JPanel rbDefaultPanel = new JPanel(new FlowLayout());
    private final TimeSeriesCollection tsDefaultPanel = new TimeSeriesCollection();
    private final JSpinner daySpinnerDefaultPanel = new JSpinner(new SpinnerNumberModel(DFPanel.defaultDaysHistory, 0, 2, 1));
    private final JSpinner hourSpinnerDefaultPanel = new JSpinner(new SpinnerNumberModel(DFPanel.defaultHoursHistory, 0, 23, 1));
    private final JSpinner minutesSpinnerDefaultPanel = new JSpinner(new SpinnerNumberModel(DFPanel.defaultMinutesHistory, 0, 59, 1));
    private final ChangeListener spinnerListenerDefaultPanel;

    private final JPanel rbOtherPanel = new JPanel(new FlowLayout());
    private final TimeSeriesCollection tsOtherPanel = new TimeSeriesCollection();
    private final JSpinner daySpinnerOtherPanel = new JSpinner(new SpinnerNumberModel(DFPanel.defaultDaysHistory, 0, 2, 1));
    private final JSpinner hourSpinnerOtherPanel = new JSpinner(new SpinnerNumberModel(DFPanel.defaultHoursHistory, 0, 23, 1));
    private final JSpinner minutesSpinnerOtherPanel = new JSpinner(new SpinnerNumberModel(DFPanel.defaultMinutesHistory, 0, 59, 1));
    private final ChangeListener spinnerListenerOtherPanel;

    /**
     * Time period for time series.
     */
    private final static Class<? extends RegularTimePeriod> tsTimePeriod = Millisecond.class;

    private final static String unknownString = "Unknown";

    /**
     * This class contains the description of a quantity to monitor (as defined in the DB).
     * <p>
     * Each object of this class is defined by IS server names, the IS information names and the IS attribute names for rates and event
     * counters; an additional attribute is the information description.
     * <p>
     * An object of this class is immutable and can be safely shared amongst various threads. Whenever this object will no more be immutable
     * consider to redesign the thread policy access to such an object.
     */
    private static class ISInfoSource {
        private final String rateServerName;
        private final String rateInfoName;
        private final String rateAttributeName;
        private final StringBuilder rateFullInfoName = new StringBuilder();

        private final String counterServerName;
        private final String counterInfoName;
        private final String counterAttributeName;
        private final StringBuilder counterFullInfoName = new StringBuilder();

        private final String infoDescription;

        /**
         * Constructor
         * 
         * @param rateServerName "Rate" IS server name
         * @param rateInfoName "Rate" IS information name
         * @param rateAttributeName "Rate" IS information attribute name
         * @param counterServerName "Counter" IS server name
         * @param counterInfoName "Counter" IS server name
         * @param counterAttributeName "Counter" IS information attribute name
         * @param description Information description
         */
        public ISInfoSource(final String rateServerName,
                            final String rateInfoName,
                            final String rateAttributeName,
                            final String counterServerName,
                            final String counterInfoName,
                            final String counterAttributeName,
                            final String description)
        {
            this.rateServerName = rateServerName;
            this.rateInfoName = rateInfoName;
            this.rateAttributeName = rateAttributeName;
            // If all the attributes are empty do not build the full info name (otherwise it
            // will be made only of dots).
            if((this.rateServerName.isEmpty() == false) || (this.rateInfoName.isEmpty() == false)
               || (this.rateAttributeName.isEmpty() == false))
            {
                this.rateFullInfoName.append(this.rateServerName + "." + this.rateInfoName + "." + this.rateAttributeName);
            }

            this.counterServerName = counterServerName;
            this.counterInfoName = counterInfoName;
            this.counterAttributeName = counterAttributeName;
            // If all the attributes are empty do not build the full info name (otherwise it
            // will be made only of dots).
            if((this.counterServerName.isEmpty() == false) || (this.counterInfoName.isEmpty() == false)
               || (this.counterAttributeName.isEmpty() == false))
            {
                this.counterFullInfoName.append(this.counterServerName + "." + this.counterInfoName + "." + this.counterAttributeName);
            }

            this.infoDescription = description;
        }

        /**
         * Constructor.
         * <p>
         * All the fields but the description are at their defaults.
         * 
         * @param description The information description
         */
        public ISInfoSource(final String description) {
            this(DFPanel.defServerName,
                 DFPanel.defInfoName,
                 DFPanel.defRateAttribute,
                 DFPanel.defServerName,
                 DFPanel.defInfoName,
                 DFPanel.defCounterAttribute,
                 description);
        }

        public final String getRateServerName() {
            return this.rateServerName;
        }

        public final String getRateInfoName() {
            return this.rateInfoName;
        }

        public final String getRateAttributeName() {
            return this.rateAttributeName;
        }

        public final String getRateFullInfoName() {
            return this.rateFullInfoName.toString();
        }

        public final String getCounterServerName() {
            return this.counterServerName;
        }

        public final String getCounterInfoName() {
            return this.counterInfoName;
        }

        public final String getCounterAttributeName() {
            return this.counterAttributeName;
        }

        public final String getCounterFullInfoName() {
            return this.counterFullInfoName.toString();
        }

        public final String getInfoDescription() {
            return this.infoDescription;
        }
    }

    /**
     * Abstract class encapsulating IS receivers for rates and event counters objects.
     * <p>
     * The class holds two {@link is.Receiver} references to get updates both for the event counter and the rates. Subclasses need to
     * override {@link CountersAndRatesReceiver#processCounterInfoUpdate(AnyInfo, int)} and
     * {@link CountersAndRatesReceiver#processRateInfoUpdate(AnyInfo, int)} to correctly process IS events for rates and event counters.
     * Those methods will be called any time the relevant IS information is created or updated (but not when it is removed).
     */
    private abstract static class CountersAndRatesReceiver {
        /**
         * The {@link ISInfoSource} object holding the rate and event counters description
         */
        private final ISInfoSource isInfoSource;

        /**
         * Reference to the IPC partition
         */
        private final ipc.Partition ipcPartition;

        /**
         * Receiver to process 'rate' updates
         */
        private final is.Receiver rateReceiver;

        /**
         * Receiver to process 'counter' updates
         */
        private final is.Receiver counterReceiver;

        /**
         * Class implementing {@link is.InfoListener} to properly process IS call-backs.
         * <p>
         * It overrides {@link is.Receiver#infoCreated(InfoEvent)} and {@link is.Receiver#infoUpdated(InfoEvent)}.
         */
        private abstract static class ISReceiver extends is.Receiver {
            /**
             * The index of the IS information attribute
             */
            // We do not know a priori the IS information type and we need to calculate
            // the attribute index to be used with AnyInfo.
            private final AtomicInteger attributeIndex = new AtomicInteger(-1);

            /**
             * The IS server name
             */
            private final String serverName;

            /**
             * The IS information name
             */
            private final String infoName;

            /**
             * The information attribute index
             */
            private final String attributeName;

            /**
             * Reference to the IPC partition
             */
            private final ipc.Partition partition;

            /**
             * Constructor.
             * 
             * @param part Reference to the IPC partition
             * @param serverName IS server name
             * @param infoName IS information name
             * @param attributeName IS information attribute name
             */
            ISReceiver(final ipc.Partition part, final String serverName, final String infoName, final String attributeName) {
                super(part, serverName + "." + infoName, true);

                this.serverName = serverName;
                this.infoName = infoName;
                this.attributeName = attributeName;

                this.partition = part;
            }

            /**
             * It calls {@link #callbackReceived(InfoEvent)}.
             * 
             * @see is.Receiver#infoCreated(is.InfoEvent)
             */
            @Override
            public final void infoCreated(final InfoEvent info) {
                this.callbackReceived(info);
            }

            /**
             * It calls {@link #callbackReceived(InfoEvent)}.
             * 
             * @see is.Receiver#infoUpdated(is.InfoEvent)
             */
            @Override
            public final void infoUpdated(final InfoEvent info) {
                this.callbackReceived(info);
            }

            /**
             * This method is called any time the IS information we are interested in is updated. The arguments are the {@link is.AnyInfo}
             * information object and the index of the interesting attribute (with rate and counters only one information attribute is
             * important, i.e., the rate or the counter value).
             * 
             * @param info Updated information
             * @param infoIndex The index of the information attribute we are interested in
             */
            protected abstract void processInfoUpdate(final is.AnyInfo info, final int infoIndex);

            /**
             * It creates an {@link is.AnyInfo} object from the {@link is.InfoEvent}.
             * 
             * @param info The {@link is.InfoEvent} from the call-back
             * @return The {@link is.AnyInfo} object
             */
            private is.AnyInfo getInfo(final InfoEvent info) {
                final is.AnyInfo isInfo = new is.AnyInfo();
                info.getValue(isInfo);
                return isInfo;
            }

            /**
             * It calculates the information attribute index and then calls {@link #processInfoUpdate(AnyInfo, int)}.
             * 
             * @param info The call-back info event
             * @see #getAttributeIndex(AnyInfo)
             * @see #getInfo(InfoEvent)
             */
            private void callbackReceived(final InfoEvent info) {
                final is.AnyInfo isInfo = this.getInfo(info);
                final int index = this.getAttributeIndex(isInfo);
                if(index >= 0) {
                    this.processInfoUpdate(isInfo, index);
                }
            }

            /**
             * It finds the index of attribute {@link #attributeName} for the IS information {@link #infoName}.
             * 
             * @param isInfo The IS information object causing the call-back event
             * @return The (positive) index of the {@link #attributeName} attribute or a negative value if the attribute index could not be
             *         found.
             */
            private final int getAttributeIndex(final is.AnyInfo isInfo) {
                int valueToReturn = -2;

                // Check if the attribute index has already been calculated, do not do
                // it every time a call-back is received
                if(this.attributeIndex.get() == -1) {
                    // -1 means that we are here for the first time
                    // or all the other times the index cannot be found
                    // because of an error communicating with the IS server (CORBA timeout)
                    try {
                        boolean found = false;

                        // Loop over the is.InfoDocument to find the attribute index
                        final is.InfoDocument isInfoDoc = new is.InfoDocument(this.partition, isInfo);
                        final int attrCount = isInfoDoc.getAttributeCount();
                        for(int j = 0; j < attrCount; ++j) {
                            final is.InfoDocument.Attribute attr = isInfoDoc.getAttribute(j);
                            if(attr.getName().equals(this.attributeName)) {
                                valueToReturn = j;
                                found = true;
                                break;
                            }
                        }

                        if(found == false) {
                            final String msg = "The info \"" + this.serverName + "." + this.infoName + "." + this.attributeName
                                               + "\" cannot be found in the IS repository; the associated counter will not be updated";
                            IguiLogger.error(msg);
                        }
                    }
                    catch(final Exception ex) {
                        final String msg = "An error occurred while getting the IS info \"" + this.serverName + "." + this.infoName + "."
                                           + this.attributeName + "\": " + ex + ". The associated counter will not be correctly updated";
                        IguiLogger.error(msg, ex);

                        final Throwable exCause = ex.getCause();
                        if(org.omg.CORBA.TIMEOUT.class.isInstance(exCause)) {
                            valueToReturn = -1;
                        }
                    }

                    // At this point the value can be:
                    // > 0 - The index has been found
                    // -1 - CORBA timeout talking to the server (we should try again)
                    // -2 - The information is not found (i.e., we are looking for something which does not exist, do not try again)
                    this.attributeIndex.set(valueToReturn);
                } else {
                    // Here the value can be:
                    // > 0 - The index has been found
                    // -2 - Attribute not found
                    valueToReturn = this.attributeIndex.get();
                }

                return valueToReturn;
            }
        }

        /**
         * Constructor.
         * 
         * @param ipcPartition Reference to the IPC partition
         * @param isSource The {@link ISInfoSource} object containing the description of the IS information for rate and event counters.
         */
        CountersAndRatesReceiver(final ipc.Partition ipcPartition, final ISInfoSource isSource) {
            this.isInfoSource = isSource;
            this.ipcPartition = ipcPartition;

            // Receiver for event rates
            this.rateReceiver = new ISReceiver(this.ipcPartition,
                                               this.isInfoSource.getRateServerName(),
                                               this.isInfoSource.getRateInfoName(),
                                               this.isInfoSource.getRateAttributeName())
            {
                @Override
                protected void processInfoUpdate(final AnyInfo info, final int infoIndex) {
                    CountersAndRatesReceiver.this.processRateInfoUpdate(info, infoIndex);
                }
            };

            // Receiver for event counters
            this.counterReceiver = new ISReceiver(this.ipcPartition,
                                                  this.isInfoSource.getCounterServerName(),
                                                  this.isInfoSource.getCounterInfoName(),
                                                  this.isInfoSource.getCounterAttributeName())
            {
                @Override
                protected void processInfoUpdate(final AnyInfo info, final int infoIndex) {
                    CountersAndRatesReceiver.this.processCounterInfoUpdate(info, infoIndex);
                }
            };
        }

        /**
         * Override this method to process IS call-back events for 'rates'.
         * 
         * @param info The updates IS information
         * @param infoIndex The index of the IS information attribute we are interested in
         */
        protected abstract void processRateInfoUpdate(final is.AnyInfo info, final int infoIndex);

        /**
         * Override this method to process IS call-back events for 'counters'.
         * 
         * @param info The updates IS information
         * @param infoIndex The index of the IS information attribute we are interested in
         */
        protected abstract void processCounterInfoUpdate(final is.AnyInfo info, final int infoIndex);

        /**
         * Subscribe to IS for 'rates' information.
         * 
         * @throws DFPanelExceptions.ISException An error occurred trying to perform the IS subscription
         */
        protected void subscribeToRates() throws DFPanelExceptions.ISException {
            try {
                this.rateReceiver.subscribe();
            }
            catch(final Exception ex) {
                final String errMsg = "Failed subscribing to IS for information \"" + this.isInfoSource.getRateFullInfoName() + "\": " + ex;
                throw new DFPanelExceptions.ISException(errMsg, ex);
            }
        }

        /**
         * Subscribe to IS for 'counters' information.
         * 
         * @throws DFPanelExceptions.ISException An error occurred trying to perform the IS subscription
         */
        protected void subscribeToCounters() throws DFPanelExceptions.ISException {
            try {
                this.counterReceiver.subscribe();
            }
            catch(final Exception ex) {
                final String errMsg = "Failed subscribing to IS for information \"" + this.isInfoSource.getCounterFullInfoName() + "\": "
                                      + ex;
                throw new DFPanelExceptions.ISException(errMsg, ex);
            }
        }

        /**
         * It performs and IS subscription for both 'counters' and 'rates'.
         * <p>
         * It calls {@link #subscribeToCounters()} and {@link #subscribeToRates()}.
         */
        public void subscribe() {
            try {
                this.subscribeToRates();
            }
            catch(final DFPanelExceptions.ISException ex) {
                IguiLogger.error(ex.getMessage(), ex);
            }
            finally {
                try {
                    this.subscribeToCounters();
                }
                catch(final DFPanelExceptions.ISException ex) {
                    IguiLogger.error(ex.getMessage(), ex);
                }
            }
        }

        /**
         * It removes the IS subscription for 'rates'
         * 
         * @throws DFPanelExceptions.ISException An error occurred while removing the IS subscription (no error is raised if the
         *             subscription does not exist).
         */
        protected void unsubscribeFromRates() throws DFPanelExceptions.ISException {
            try {
                this.rateReceiver.unsubscribe();
            }
            catch(final is.SubscriptionNotFoundException ex) {
                IguiLogger.debug(ex.toString(), ex);
            }
            catch(final Exception ex) {
                final String errMsg = "Failed unsubscribing from IS for information \"" + this.isInfoSource.getRateFullInfoName() + "\": "
                                      + ex;
                throw new DFPanelExceptions.ISException(errMsg, ex);
            }
        }

        /**
         * It removes the IS subscription for 'counters'
         * 
         * @throws DFPanelExceptions.ISException An error occurred while removing the IS subscription (no error is raised if the
         *             subscription does not exist).
         */
        protected void unsubscribeFromCounters() throws DFPanelExceptions.ISException {
            try {
                this.counterReceiver.unsubscribe();
            }
            catch(final is.SubscriptionNotFoundException ex) {
                IguiLogger.debug(ex.toString(), ex);
            }
            catch(final Exception ex) {
                final String errMsg = "Failed unsubscribing from IS for information \"" + this.isInfoSource.getCounterFullInfoName()
                                      + "\": " + ex;
                throw new DFPanelExceptions.ISException(errMsg, ex);
            }

        }

        /**
         * It removes the IS subscription for both 'counters' and 'rates'.
         * <p>
         * It calls {@link #unsubscribeFromCounters()} and {@link #unsubscribeFromRates()}.
         */
        public void unsubscribe() {
            try {
                this.unsubscribeFromRates();
            }
            catch(final DFPanelExceptions.ISException ex) {
                IguiLogger.error(ex.getMessage(), ex);
            }
            finally {
                try {
                    this.unsubscribeFromCounters();
                }
                catch(final DFPanelExceptions.ISException ex) {
                    IguiLogger.error(ex.getMessage(), ex);
                }
            }
        }
    }

    /**
     * Class extending {@link CountersAndRatesReceiver} used to get and process information coming from the IS server and holding rate and
     * event counter values.
     * <p>
     * This class holds a {@link TimeSeries} and a {@link JCheckBox}: the general contract is that every instance of this class will
     * properly fill the time series using the information coming from the IS server; the check box will be used by the enclosing class to
     * allow the time series to be added or removed from the chart when the check box itself is selected or de-selected. Using this approach
     * this class has no direct access to the chart and it its only task is to keep updated and in a consistent state the time series. The
     * time period for time series updates is defined by {@link DFPanel#tsTimePeriod}.
     * <p>
     * The time series has to be filled with values for rates as defined by the {@link ISInfoSource} passed as an argument to the
     * constructor. The class may also be asked (at construction time) to keep trace of event counter values: in this case the value will be
     * reported updating the name associated to the check box.
     * <p>
     * Time series have to be created with meaningful name and domain description to avoid misidentification ({@link PlotRenderer}).
     * 
     * @see DFPanel#timeSeriesSelected(RateReceiver, TimeSeriesCollection)
     */
    private static class RateReceiver extends CountersAndRatesReceiver {
        /**
         * The time series
         */
        private final TimeSeries ts;

        /**
         * The check box
         */
        private final JCheckBox rb;

        /**
         * Description of the information to retrieve from IS
         */
        private final ISInfoSource isSource;

        /**
         * The name (description)
         */
        private final String name;

        /**
         * Whether to show or not the event counter value
         */
        private final boolean showCounter;

        /**
         * Constructor.
         * <p>
         * To be executed in the EDT because some Swing components are initialized.
         * <p>
         * All the (un)subscription methods should be executed outside the EDT because they imply remote calls to the database server.
         * 
         * @param ipcPartition The IPC partition
         * @param isSource The {@link ISInfoSource} containing the IS server information name
         * @param showCounter <code>true</code> if the event counter value should be shown as well
         */
        RateReceiver(final Partition ipcPartition, final ISInfoSource isSource, final boolean showCounter) {
            super(ipcPartition, isSource);

            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "Method to be executed in the EDT!";

            this.isSource = isSource;
            this.name = this.isSource.getInfoDescription();
            this.showCounter = showCounter;

            this.ts = new TimeSeries(this.name, this.isSource.getRateFullInfoName(), "");
            
            this.rb = new JCheckBox(this.name, false) {
                private static final long serialVersionUID = -8547238671380354707L;

                @Override
                public JToolTip createToolTip() {
                    final MultiLineToolTip tt = new MultiLineToolTip(true);
                    tt.setComponent(this);
                    return tt;
                }
            };
            this.rb.setToolTipText("<html><b>Event Counter:</b> " + this.isSource.getCounterFullInfoName() + "<br /><b>Rate:</b> "
                                   + this.isSource.getRateFullInfoName() + "</html>");
        }

        /**
         * @see IguiPanels.DFPanel.DFPanel.CountersAndRatesReceiver#subscribeToRates()
         */
        @Override
        protected void subscribeToRates() throws ISException {
            if(this.isSource.getRateFullInfoName().isEmpty() == false) {
                super.subscribeToRates();
                IguiLogger.debug("Subscribed to IS for information: \"" + this.isSource.getRateFullInfoName() + "\"");
            } else {
                IguiLogger.warning("I will not subscribe to rate counter for \"" + this.name
                                   + "\": the IS information description is empty");
            }
        }

        /**
         * @see IguiPanels.DFPanel.DFPanel.CountersAndRatesReceiver#subscribeToCounters()
         */
        @Override
        protected void subscribeToCounters() throws ISException {
            if(RateReceiver.this.showCounter == true) {
                if(this.isSource.getCounterFullInfoName().isEmpty() == false) {
                    super.subscribeToCounters();
                    IguiLogger.debug("Subscribed to IS for information: \"" + this.isSource.getCounterFullInfoName() + "\"");
                } else {
                    IguiLogger.warning("I will not subscribe to event counter for \"" + this.name
                                       + "\": the IS information description is empty");
                }
            }
        }

        /**
         * @see IguiPanels.DFPanel.DFPanel.CountersAndRatesReceiver#unsubscribeFromCounters()
         */
        @Override
        protected void unsubscribeFromCounters() throws ISException {
            if(RateReceiver.this.showCounter == true) {
                if(this.isSource.getCounterAttributeName().isEmpty() == false) {
                    super.unsubscribeFromCounters();
                    IguiLogger.debug("Removed IS subscription for information: \"" + this.isSource.getCounterFullInfoName() + "\"");
                } else {
                    IguiLogger.debug("I will not unsubscribe from event counter for \"" + this.name
                                     + "\": the IS information description is empty");
                }
            }
        }

        /**
         * @see IguiPanels.DFPanel.DFPanel.CountersAndRatesReceiver#unsubscribeFromRates()
         */
        @Override
        protected void unsubscribeFromRates() throws ISException {
            if(this.isSource.getRateFullInfoName().isEmpty() == false) {
                super.unsubscribeFromRates();
                IguiLogger.debug("Removed IS subscription for information: \"" + this.isSource.getRateFullInfoName() + "\"");
            } else {
                IguiLogger.debug("I will not unsubscribe from rate counter for \"" + this.name
                                 + "\": the IS information description is empty");
            }
        }

        /**
         * It return a reference to the check box.
         * 
         * @return A reference to the check box
         */
        public JCheckBox getCheckBox() {
            return this.rb;
        }

        /**
         * It returns a reference to the time series.
         * 
         * @return A reference to the time series
         */
        public TimeSeries getTimeSeries() {
            return this.ts;
        }

        /**
         * If {@link #showCounter} is <code>true</code> then the event counter value is added to the check box label.
         * 
         * @see IguiPanels.DFPanel.DFPanel.CountersAndRatesReceiver#processCounterInfoUpdate(is.AnyInfo, int)
         */
        @Override
        protected void processCounterInfoUpdate(final AnyInfo info, final int infoIndex) {
            if(this.showCounter == true) {
                try {
                    final Object infoValue = info.getAttribute(infoIndex);
                    final byte infoValueType = info.getAttributeType(infoIndex);

                    final String infoValueString;
                    switch(infoValueType) {
                        case is.Type.U8:
                            infoValueString = Integer.toString(Byte.toUnsignedInt(Byte.parseByte(infoValue.toString())));
                            break;
                        case is.Type.U16:
                            infoValueString = Integer.toString(Short.toUnsignedInt(Short.parseShort(infoValue.toString())));
                            break;
                        case is.Type.U32:
                            infoValueString = Long.toString(Integer.toUnsignedLong(Integer.parseInt(infoValue.toString())));
                            break;
                        default:
                            infoValueString = infoValue.toString();
                    }

                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            RateReceiver.this.rb.setText(RateReceiver.this.name + " (" + infoValueString + ") ");
                        }
                    });
                }
                catch(final Exception ex) {
                    final String errMsg = "Failed processing IS update for \"" + RateReceiver.this.isSource.getInfoDescription() + "\": "
                                          + ex;
                    IguiLogger.error(errMsg, ex);

                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            RateReceiver.this.rb.setText(RateReceiver.this.name + " (NaN) ");
                        }
                    });
                }
            }
        }

        /**
         * The rate value is added to the time series.
         * 
         * @see IguiPanels.DFPanel.DFPanel.CountersAndRatesReceiver#processRateInfoUpdate(is.AnyInfo, int)
         */
        @Override
        protected void processRateInfoUpdate(final AnyInfo info, final int infoIndex) {
            try {
                final Date infoTime = info.getTime();
                final Object infoValue = info.getAttribute(infoIndex);
                final byte infoValueType = info.getAttributeType(infoIndex);

                final String infoValueString;
                switch(infoValueType) {
                    case is.Type.U8:
                        infoValueString = Integer.toString(Byte.toUnsignedInt(Byte.parseByte(infoValue.toString())));
                        break;
                    case is.Type.U16:
                        infoValueString = Integer.toString(Short.toUnsignedInt(Short.parseShort(infoValue.toString())));
                       break;
                    case is.Type.U32:
                        infoValueString = Long.toString(Integer.toUnsignedLong(Integer.parseInt(infoValue.toString())));
                        break;
                    default:
                        infoValueString = infoValue.toString();
                }

                final double infoValueDouble = Double.parseDouble(infoValueString);

                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            RateReceiver.this.ts.addOrUpdate(RegularTimePeriod.createInstance(DFPanel.tsTimePeriod,
                                                                                              infoTime,
                                                                                              TimeZone.getDefault(),
                                                                                              Locale.getDefault()),
                                                             infoValueDouble);
                        }
                        catch(final Exception ex) {
                            final String msg = "Failed updating plot for \"" + RateReceiver.this.isSource.getInfoDescription() + "\": "
                                               + ex;
                            IguiLogger.error(msg, ex);
                        }
                    }
                });
            }
            catch(final Exception ex) {
                final String msg = "Failed processing IS update for \"" + RateReceiver.this.isSource.getInfoDescription() + "\": " + ex;
                IguiLogger.error(msg, ex);
            }
        }

        /**
         * It clears the time series and the event counter.
         * 
         * @see DFPanel#subscribeAll(boolean, boolean)
         */
        void reset() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

            this.ts.clear();
            this.rb.setText(this.name);
        }
    }

    /**
     * Renderer for the chart.
     * <p>
     * It is used to be sure that each time series is shown with the right color and shapes.
     */
    // The default implementation associates a color to a time series taking into account the order in which
    // a time series is added to the time series collections. Example: suppose to have 3 time series; the renderer
    // will associate Color1 to the 1st series, Color2 to the 2nd series and Color3 to the third series. If you
    // remove the 2nd and the 3rd time series and them add them in the opposite order (first the 3rd and then the
    // 2nd time series) then the renderer will associate Color2 to the 3rd time series and Color3 to the
    // 2nd time series. We want to modify this behavior in a more consistent way. The trick is to let the
    // default renderer implementation assign the color to a time series the first time it is added to the chart,
    // keep note of the Color-TimeSeries association and use the superclass only when a DIFFERENT time series
    // is added to the chart. The same observations are valid for time series shapes.
    private static class PlotRenderer extends StandardXYItemRenderer implements DatasetChangeListener {
        private static final long serialVersionUID = 9100841084054348246L;

        /**
         * Map associating some property to each time series.
         * <p>
         * The property includes color and shape.
         */
        private final Map<String, SeriesProperties> cm = new HashMap<String, SeriesProperties>();

        /**
         * The time series collection (it is the time series collector)
         */
        private final TimeSeriesCollection tsc;

        // The following counters keep trace of the number of times the renderer is asked about a time series
        // not already contained in the map. These numbers are used to ask the base class to return the color
        // and the shape associated the 'n' time series.
        int colorCounter = 0;
        int shapeCounter = 0;

        /**
         * Simple class holding the Paint (i.e., Color) and the Shape associated to each time series.
         */
        private static class SeriesProperties {
            private Paint linePaint;
            private Shape markerShape;

            SeriesProperties(final Paint paint, final Shape shape) {
                this.linePaint = paint;
                this.markerShape = shape;
            }

            public final Paint getLinePaint() {
                return this.linePaint;
            }

            public final void setLinePaint(final Paint linePaint) {
                this.linePaint = linePaint;
            }

            public final Shape getMarkerShape() {
                return this.markerShape;
            }

            public final void setMarkerShape(final Shape markerShape) {
                this.markerShape = markerShape;
            }
        }

        /**
         * Constructor.
         * 
         * @param tsc Reference to the {@link TimeSeriesCollection} holding all the time series.
         */
        PlotRenderer(final TimeSeriesCollection tsc) {
            super();

            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "To be executed in the EDT!";

            this.setBaseShapesVisible(true);
            this.tsc = tsc;
            this.tsc.addChangeListener(this);
        }

        /**
         * @see org.jfree.data.general.DatasetChangeListener#datasetChanged(org.jfree.data.general.DatasetChangeEvent)
         */
        @Override
        public void datasetChanged(final DatasetChangeEvent event) {
        }

        /**
         * It updates the map and notifies all the listeners.
         * 
         * @see org.jfree.chart.renderer.AbstractRenderer#setSeriesPaint(int, java.awt.Paint, boolean)
         */
        @Override
        public void setSeriesPaint(final int series, final Paint paint, final boolean notify) {
            final String key = this.getTimeSeriesId(series);

            SeriesProperties sp = this.cm.get(key);
            if(sp != null) {
                sp.setLinePaint(paint);
            } else {
                sp = new SeriesProperties(paint, null);
                this.cm.put(key, sp);
            }

            if(notify == true) {
                this.notifyListeners(new RendererChangeEvent(this));
            }
        }

        /**
         * If the paint for the time series specified by <code>series</code> is in the map then the cached value is used, otherwise the
         * super class is asked to create the Paint object.
         * 
         * @see org.jfree.chart.renderer.AbstractRenderer#getSeriesPaint(int)
         */
        @Override
        public Paint getSeriesPaint(final int series) {
            // Get the time series at index 'series': the index refers to the time series insertion
            // order in the time series collection
            final String key = this.getTimeSeriesId(series);

            // See if we already have a paint for that series
            Paint p;
            SeriesProperties sp = this.cm.get(key);
            if(sp != null) {
                p = sp.getLinePaint();
                if(p == null) {
                    // Ask the superclass and increment the counter: this is a trick
                    // to tell the superclass to give me the 'next' available color
                    p = super.getSeriesPaint(this.colorCounter++);
                    sp.setLinePaint(p);
                }
            } else {
                // Ask the superclass and increment the counter: this is a trick
                // to tell the superclass to give me the 'next' available color
                p = super.getSeriesPaint(this.colorCounter++);
                sp = new SeriesProperties(p, null);
                this.cm.put(key, sp);
            }

            return p;
        }

        /**
         * Same as {@link #getSeriesPaint(int)} but for Shapes.
         * 
         * @see org.jfree.chart.renderer.AbstractRenderer#getSeriesShape(int)
         */
        @Override
        public Shape getSeriesShape(final int series) {
            final String key = this.getTimeSeriesId(series);

            Shape s;
            SeriesProperties sp = this.cm.get(key);
            if(sp != null) {
                s = sp.getMarkerShape();
                if(s == null) {
                    s = super.getSeriesShape(this.shapeCounter++);
                    sp.setMarkerShape(s);
                }
            } else {
                s = super.getSeriesShape(this.shapeCounter++);
                sp = new SeriesProperties(null, s);
                this.cm.put(key, sp);
            }

            return s;
        }

        /**
         * Same as {@link #setSeriesPaint(int, Paint)} but for Shapes.
         * 
         * @see org.jfree.chart.renderer.AbstractRenderer#setSeriesShape(int, java.awt.Shape, boolean)
         */
        @Override
        public void setSeriesShape(final int series, final Shape shape, final boolean notify) {
            final String key = this.getTimeSeriesId(series);

            SeriesProperties sp = this.cm.get(key);
            if(sp != null) {
                sp.setMarkerShape(shape);
            } else {
                sp = new SeriesProperties(null, shape);
                this.cm.put(key, sp);
            }

            if(notify == true) {
                this.notifyListeners(new RendererChangeEvent(this));
            }
        }

        /**
         * It returns the identification string of the time series at index <code>index</code>.
         * <p>
         * The identification string is built using the time series key and the domain description.
         * 
         * @param index The time series index
         * @return The string representing the time series
         */
        private String getTimeSeriesId(final int index) {
            final TimeSeries ts = this.tsc.getSeries(index);
            return ts.getKey() + "-" + ts.getDomainDescription();
        }
    }

    // Initializer
    {
        // Set spinner listeners for both panels
        this.spinnerListenerDefaultPanel = new ChangeListener() {
            @Override
            public void stateChanged(final ChangeEvent e) {
                DFPanel.this.setStandardTimeSeriesHistory();
            }
        };

        this.spinnerListenerOtherPanel = new ChangeListener() {
            @Override
            public void stateChanged(final ChangeEvent e) {
                DFPanel.this.setOtherTimeSeriesHistory();
            }
        };

        this.helpFilName = "file:" + System.getenv("TDAQ_INST_PATH") + "/share/data/IguiPanels/OnlineHelp/DFPanel.htm";

        // Build the GUI
        this.initGUI();
    }

    /**
     * Constructor.
     * 
     * @param mainIgui Reference to the main Igui
     */
    public DFPanel(final Igui mainIgui) {
        super(mainIgui);

        this.mainIgui = mainIgui;
    }

    /**
     * @see Igui.IguiPanel#getPanelName()
     */
    @Override
    public String getPanelName() {
        return DFPanel.pName;
    }

    /**
     * @see Igui.IguiPanel#getTabName()
     */
    @Override
    public String getTabName() {
        return DFPanel.pName;
    }

    /**
     * It initializes the panel creating the needed IS receivers and setting up the charts (cleaning them as weel, because this method is
     * called also every time the database is reloaded).
     * 
     * @see Igui.IguiPanel#panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     */
    @Override
    public void panelInit(final State rcState, final AccessControlStatus controlStatus) {
        // Create new receivers: keep note that the already existing lists are not modified!
        // But the references are updated (the are volatile!)
        // A direct modification of the lists would imply additional synchronization
        this.fixedReceivers = this.createFixedReceivers();
        this.otherReceivers = this.createAdditionalReceivers();

        // Subscribe all the receivers to IS if needed
        if(rcState.follows(DFPanel.stateToSubscribe) == true) {
            this.subscribeAll(true, true);
        }

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Add listeners to the radio buttons
                for(final RateReceiver rr : DFPanel.this.fixedReceivers) {
                    rr.getCheckBox().addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(final ActionEvent e) {
                            DFPanel.this.timeSeriesSelected(rr, DFPanel.this.tsDefaultPanel);
                        }
                    });
                }

                for(final RateReceiver rr : DFPanel.this.otherReceivers) {
                    rr.getCheckBox().addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(final ActionEvent e) {
                            DFPanel.this.timeSeriesSelected(rr, DFPanel.this.tsOtherPanel);
                        }
                    });
                }

                // Clean the panels and add the check boxes
                DFPanel.this.rbDefaultPanel.removeAll();
                DFPanel.this.tsDefaultPanel.removeAllSeries();
                for(final RateReceiver rr : DFPanel.this.fixedReceivers) {
                    DFPanel.this.rbDefaultPanel.add(rr.getCheckBox());
                }
                DFPanel.this.rbDefaultPanel.revalidate();
                DFPanel.this.rbDefaultPanel.repaint();

                DFPanel.this.rbOtherPanel.removeAll();
                DFPanel.this.tsOtherPanel.removeAllSeries();
                for(final RateReceiver rr : DFPanel.this.otherReceivers) {
                    DFPanel.this.rbOtherPanel.add(rr.getCheckBox());
                }
                DFPanel.this.rbOtherPanel.revalidate();
                DFPanel.this.rbOtherPanel.repaint();

                // Set the maximum time to store in the time series
                DFPanel.this.setStandardTimeSeriesHistory();
                DFPanel.this.setOtherTimeSeriesHistory();
            }
        });
    }

    /**
     * All the IS subscriptions are removed
     * 
     * @see Igui.IguiPanel#panelTerminated()
     */
    @Override
    public void panelTerminated() {
        this.subscribeAll(false, true);
    }

    /**
     * This panel is RC state aware.
     * 
     * @see Igui.IguiPanel#rcStateAware()
     * @see DFPanel#rcStateChanged(State, State)
     */
    @Override
    public boolean rcStateAware() {
        return true;
    }

    /**
     * IS subscriptions are removed and the panel is re-initialized.
     * 
     * @see Igui.IguiPanel#dbReloaded()
     */
    @Override
    public void dbReloaded() {
        this.subscribeAll(false, false);
        this.panelInit(this.mainIgui.getRCState(), this.mainIgui.getAccessLevel());
    }

    /**
     * IS subscriptions are performed or removed taking into account the RC state.
     * 
     * @see Igui.IguiPanel#rcStateChanged(Igui.RunControlFSM.State, Igui.RunControlFSM.State)
     */
    @Override
    public void rcStateChanged(final State oldState, final State newState) {
        try {
            final Set<RunControlFSM.Transition> tr = RunControlFSM.instance().from(oldState, newState);

            if(tr.contains(DFPanel.transitionToSubscribe)) {
                this.subscribeAll(true, true);
            }

            if(tr.contains(DFPanel.transitionToUnsubscribe)) {
                this.subscribeAll(false, false);
            }
        }
        catch(final UnknownTransition ex) {
            final String errMsg = "Failed to react to an RC state change (from " + oldState.name() + " to " + newState.name() + "): " + ex;
            IguiLogger.error(errMsg, ex);
            this.mainIgui.internalMessage(MessageSeverity.ERROR, errMsg);
        }
        catch(final IllegalArgumentException ex) {
            IguiLogger.warning(ex.getMessage(), ex);
        }
    }

    /**
     * IS subscriptions are removed and performed again.
     * 
     * @see Igui.IguiPanel#refreshISSubscription()
     */
    @Override
    public void refreshISSubscription() {
        this.subscribeAll(false, false);
        this.subscribeAll(true, false);
    }

    /**
     * Performs IS subscriptions for all the IS receivers.
     * <p>
     * Since this method performs some remote calls, it should not be used in the EDT.
     * 
     * @param subscribe If <code>true</code> the subscriptions are done, otherwise are removed.
     * @param reset If <code>true</code> the {@link RateReceiver}s are reset
     * @see RateReceiver#reset()
     */
    private void subscribeAll(final boolean subscribe, final boolean reset) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == false) : "To not be called in the EDT!";

        if(subscribe == true) {
            for(final RateReceiver rr : this.fixedReceivers) {
                rr.subscribe();
            }

            for(final RateReceiver rr : this.otherReceivers) {
                rr.subscribe();
            }
        } else {
            for(final RateReceiver rr : this.fixedReceivers) {
                rr.unsubscribe();
            }

            for(final RateReceiver rr : this.otherReceivers) {
                rr.unsubscribe();
            }
        }

        if(reset == true) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    for(final RateReceiver rr : DFPanel.this.fixedReceivers) {
                        rr.reset();
                    }

                    for(final RateReceiver rr : DFPanel.this.otherReceivers) {
                        rr.reset();
                    }
                }
            });
        }
    }

    /**
     * It creates the IS receivers for the 'Other' relationship in the database.
     * <p>
     * This method can potentially make remote calls to the database and must not be called in the EDT.
     * 
     * @return An unmodifiable list of {@link RateReceiver}s
     * @see #createInfoSource(IS_EventsAndRates)
     */
    private List<RateReceiver> createAdditionalReceivers() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == false) : "Not to be called in the EDT!";

        final List<RateReceiver> recList = new ArrayList<RateReceiver>();

        try {
            final List<ISInfoSource> infoList = new ArrayList<ISInfoSource>();

            // Get the dal partition
            final dal.Partition dalPart = dal.Partition_Helper.get(this.getDb(), this.mainIgui.getPartition().getName());
            final dal.IS_InformationSources iss = dalPart.get_IS_InformationSource();
            // Get the 'others'
            final IS_EventsAndRates isInfos[] = iss.get_Others();
            // Create the ISInfoSource objects
            for(final IS_EventsAndRates ear : isInfos) {
                infoList.add(this.createInfoSource(ear));
            }

            // Create the RateReceivers in the EDT, they access Swing components
            final FutureTask<List<RateReceiver>> task = new FutureTask<List<RateReceiver>>(new Callable<List<RateReceiver>>() {
                @Override
                public List<RateReceiver> call() throws Exception {
                    final List<RateReceiver> l = new ArrayList<RateReceiver>();

                    for(final ISInfoSource e : infoList) {
                        l.add(new RateReceiver(DFPanel.this.mainIgui.getPartition(), e, true));
                    }

                    return Collections.unmodifiableList(l);
                }
            });

            javax.swing.SwingUtilities.invokeLater(task);

            // Get the result from the task execution
            recList.addAll(task.get());
        }
        catch(final InterruptedException ex) {
            final String errMsg = "Thread interrupted!";
            Thread.currentThread().interrupt();
            IguiLogger.error(errMsg, ex);
        }
        catch(final ExecutionException ex) {
            final String errMsg = "Failed creating IS receivers for \"Others\": " + ex.getCause();
            IguiLogger.error(errMsg, ex);
        }
        catch(final Exception ex) {
            final String errMsg = "Cannot retrive the \"Others\" IS information sources from the database: " + ex;
            IguiLogger.error(errMsg, ex);
        }

        // Return an unmodifiable view of the list: this enforces the right usage of volatile list references
        return Collections.unmodifiableList(recList);
    }

    /**
     * It creates the RateReceivers for the 'standard' rate and event counters.
     * <p>
     * This method can potentially make remote calls to the database and must not be called in the EDT.
     * 
     * @return An unmodifiable list of {@link RateReceiver}s
     * @see #createInfoSource(IS_EventsAndRates, String)
     */
    private List<RateReceiver> createFixedReceivers() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == false) : "Not to be called in the EDT!";

        final List<RateReceiver> recList = new ArrayList<RateReceiver>();

        {
            final List<ISInfoSource> infoList = new ArrayList<ISInfoSource>(5);

            try {
                final dal.Partition dalPart = dal.Partition_Helper.get(this.getDb(), this.mainIgui.getPartition().getName());
                final dal.IS_InformationSources iss = dalPart.get_IS_InformationSource();
                final dal.IS_EventsAndRates lvl1 = iss.get_LVL1();
                final dal.IS_EventsAndRates hlt = iss.get_HLT();
                final dal.IS_EventsAndRates rec = iss.get_Recording();
                
                infoList.add(this.createInfoSource(lvl1, DFPanel.L1Label));
                infoList.add(this.createInfoSource(hlt, DFPanel.HLTLabel));
                infoList.add(this.createInfoSource(rec, DFPanel.RELabel));
            }
            catch(final Exception ex) {
                final String errMsg = "Cannot retrieve IS information sources from the database: " + ex
                                      + ". Default values will be used for standard event counters and rates";
                IguiLogger.warning(errMsg, ex);
                
                
                // Default names are used for standard rate and event counters; the description could be
                // empty for them but their identity is well defined
                infoList.clear();
                
                infoList.add(new ISInfoSource(DFPanel.L1Label));
                infoList.add(new ISInfoSource(DFPanel.HLTLabel));
                infoList.add(new ISInfoSource(DFPanel.RELabel));
            }

            // RateReceivers must be created in the EDT
            final FutureTask<List<RateReceiver>> task = new FutureTask<List<RateReceiver>>(new Callable<List<RateReceiver>>() {
                @Override
                public List<RateReceiver> call() throws Exception {
                    final List<RateReceiver> l = new ArrayList<RateReceiver>();

                    for(final ISInfoSource e : infoList) {
                        l.add(new RateReceiver(DFPanel.this.mainIgui.getPartition(), e, false));
                    }

                    return Collections.unmodifiableList(l);
                }
            });

            javax.swing.SwingUtilities.invokeLater(task);

            try {
                recList.addAll(task.get());
            }
            catch(final InterruptedException ex) {
                final String errMsg = "Thread interrupted!";
                Thread.currentThread().interrupt();
                IguiLogger.error(errMsg, ex);
            }
            catch(final ExecutionException ex) {
                final String errMsg = "Failed creating standard IS receivers: " + ex.getCause();
                IguiLogger.error(errMsg, ex);
            }
        }

        // Return an unmodifiable view of the list: this enforces the right usage of volatile list references
        return Collections.unmodifiableList(recList);
    }

    /**
     * It creates the {@link ISInfoSource} object holding the parameters (server, information and attribute names and description) of the IS
     * information source as it is described in the DB.
     * <p>
     * If <code>dalInfo</code> is <code>null</code> then the {@link ISInfoSource} is created using the
     * {@link ISInfoSource#ISInfoSource(String)} constructor passing it the {@link DFPanel#unknownString} string (the same string for the
     * object description is used if the <code>infoDescription</code> string is null or empty).
     * <p>
     * Usually <code>infoDescription</code> is taken from the IS object description attribute in the database, but we want to keep the
     * possibility to override it (i.e., for the standard rate and events counter which have a well defined identity even if their
     * description attribute in the database is not filled).
     * 
     * @param dalInfo The dal object describing the IS information
     * @param infoDescription A string to describe the IS information
     * @return The {@link ISInfoSource} object
     * @throws DFPanelExceptions.ConfigException Some error occurred accessing the configuration database
     * @see #tokenizeInfo(String)
     */
    private ISInfoSource createInfoSource(final dal.IS_EventsAndRates dalInfo, final String infoDescription) throws DFPanelExceptions.ConfigException {
        try {
            final ISInfoSource isis;
    
            if(dalInfo != null) {
                String description;
                if((infoDescription == null) || (infoDescription.isEmpty() == true)) {
                    description = dalInfo.get_Description().trim();
                    if(description.isEmpty() == true) {
                        description = DFPanel.unknownString;
                    }
                } else {
                    description = infoDescription;
                }
    
                final String[] rateInfo = this.tokenizeInfo(dalInfo.get_Rate());
                final String[] counterInfo = this.tokenizeInfo(dalInfo.get_EventCounter());
                isis = new ISInfoSource(rateInfo[0], rateInfo[1], rateInfo[2], counterInfo[0], counterInfo[1], counterInfo[2], description);
            } else {
                if((infoDescription == null) || (infoDescription.isEmpty() == true)) {
                    isis = new ISInfoSource(DFPanel.unknownString);
                } else {
                    isis = new ISInfoSource(infoDescription);
                }
            }
    
            return isis;
        }
        catch(final ConfigException ex) {
            throw new DFPanelExceptions.ConfigException("Cannot get IS information description from the database: " + ex, ex);
        }
    }

    /**
     * It calls {@link #createInfoSource(IS_EventsAndRates, String)} passing a <code>null</code> string.
     * @throws DFPanelExceptions.ConfigException Some error occurred accessing the configuration database
     */
    private ISInfoSource createInfoSource(final dal.IS_EventsAndRates dalInfo) throws DFPanelExceptions.ConfigException {
        return this.createInfoSource(dalInfo, null);
    }

    /**
     * It tokenizes the IS info description: from "server.this.is.the.info.name.attribute" to "server", "this.is.the.info.name", "attribute"
     * 
     * @param isInfo The full IS info name: server.info.attribute (info may contain additional dots)
     * @return An array of strings containing (in order) the IS server, information and attribute names.
     */
    private String[] tokenizeInfo(final String isInfo) {
        String[] infoTokens = new String[] {"", "", ""};

        if((isInfo != null) && (isInfo.length() > 0)) {
            // Fields are separated by dots
            final String[] tokens = isInfo.split("\\.");
            if(tokens.length >= 3) {
                // Use the tokens to extract the meaningful information: IS server, information and attribute name
                final String serverName = tokens[0];
                final String attributeName = tokens[tokens.length - 1];

                // Calculate the attribute name: it can have '.'
                final java.lang.StringBuilder pathName = new java.lang.StringBuilder();
                for(int i = 1; i < (tokens.length - 1); ++i) {
                    pathName.append(tokens[i] + ".");
                }
                final String infoName = pathName.substring(0, pathName.length() - 1); // Do not take into account last '.'

                infoTokens = new String[] {serverName, infoName, attributeName};

            } else {
                IguiLogger.warning("Invalid IS information \"" + isInfo
                                   + "\"; it must have the \"server_name.info_name.attribute_name\" format");
            }
        }

        return infoTokens;
    }

    /**
     * It sets the maximum history to keep in the 'standard' time series.
     * <p>
     * To be executed in the EDT.
     */
    private void setStandardTimeSeriesHistory() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        // Info from the spinners
        final int days = ((SpinnerNumberModel) DFPanel.this.daySpinnerDefaultPanel.getModel()).getNumber().intValue();
        final int hours = ((SpinnerNumberModel) DFPanel.this.hourSpinnerDefaultPanel.getModel()).getNumber().intValue();
        final int minutes = ((SpinnerNumberModel) DFPanel.this.minutesSpinnerDefaultPanel.getModel()).getNumber().intValue();

        final long millis = ((days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60)) * 1000L;

        for(final RateReceiver rr : DFPanel.this.fixedReceivers) {
            final TimeSeries ts = rr.getTimeSeries();
            // Set the max and remove the old items
            ts.setMaximumItemAge(millis);
            ts.removeAgedItems(true);
        }
    }

    /**
     * It sets the maximum history to keep in the 'optional' time series.
     * <p>
     * To be executed in the EDT.
     */
    private void setOtherTimeSeriesHistory() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        // Info from the spinners
        final int days = ((SpinnerNumberModel) DFPanel.this.daySpinnerOtherPanel.getModel()).getNumber().intValue();
        final int hours = ((SpinnerNumberModel) DFPanel.this.hourSpinnerOtherPanel.getModel()).getNumber().intValue();
        final int minutes = ((SpinnerNumberModel) DFPanel.this.minutesSpinnerOtherPanel.getModel()).getNumber().intValue();

        final long millis = ((days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60)) * 1000L;

        for(final RateReceiver rr : DFPanel.this.otherReceivers) {
            final TimeSeries ts = rr.getTimeSeries();
            // Set the max and remove the old items
            ts.setMaximumItemAge(millis);
            ts.removeAgedItems(true);
        }
    }

    /**
     * Action to be executed when the check box associated to each {@link RateReceiver} is selected.
     * <p>
     * The corresponding time series is added or removed from the specified time series collection.
     * <p>
     * To be executed in the EDT.
     * 
     * @param rec The rate receiver associated to the check box
     * @param tsc The time series collection holding the time series
     */
    private void timeSeriesSelected(final RateReceiver rec, final TimeSeriesCollection tsc) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        final JCheckBox rb = rec.getCheckBox();
        final TimeSeries ts = rec.getTimeSeries();

        if(rb.isSelected() == true) {
            if(tsc.getSeries(ts.getKey().toString()) == null) {
                tsc.addSeries(ts);
            }
        } else {
            if(tsc.getSeries(ts.getKey().toString()) != null) {
                tsc.removeSeries(ts);
            }
        }
    }

    /**
     * It setups the pop-up menu for the <code>panel</code> {@link ChartPanel} adding a menu item allowing\ to switch the range axis to a
     * logarithmic scale.
     * 
     * @param panel The {@link ChartPanel} whose pop-up menu will be modified
     */
    private void setPopupMenu(final ChartPanel panel) {
        // Create a menu item and add it a listener
        final JCheckBoxMenuItem logScaleItem = new JCheckBoxMenuItem("Log scale for Y");
        logScaleItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                // Get the current axis label
                final String label = panel.getChart().getXYPlot().getRangeAxis().getLabel();

                final int state = e.getStateChange();
                if(state == ItemEvent.SELECTED) {
                    // Item selected: set the log scale. The log scale should accept zero as value
                    final LogarithmicAxis logAx = new LogarithmicAxis(label);
                    logAx.setStrictValuesFlag(false);
                    logAx.setAllowNegativesFlag(true);
                    logAx.setLog10TickLabelsFlag(true);
                    logAx.setAutoRange(true);
                    logAx.setAutoTickUnitSelection(true);
                    logAx.setMinorTickMarksVisible(true);
                    logAx.setAutoRangeNextLogFlag(true);
                    panel.getChart().getXYPlot().setRangeAxis(logAx);
                    logAx.configure();
                } else if(state == ItemEvent.DESELECTED) {
                    // Item deselected: set linear scale
                    final NumberAxis linAx = new NumberAxis(label);
                    panel.getChart().getXYPlot().setRangeAxis(linAx);
                    linAx.configure();
                }

                panel.getChart().fireChartChanged();
            }
        });

        // Get the current menu item, add the new menu item and set it as the new pop-up menu
        final JPopupMenu pm = panel.getPopupMenu();
        pm.addSeparator();
        pm.add(logScaleItem);
        panel.setPopupMenu(pm);
    }

    /**
     * It creates the panel GUI.
     */
    private void initGUI() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        final ChartPanel chartDefaultPanel = new ChartPanel(ChartFactory.createTimeSeriesChart("",
                                                                                               "Time",
                                                                                               "Rate (Hz)",
                                                                                               this.tsDefaultPanel,
                                                                                               true,
                                                                                               true,
                                                                                               false));
        chartDefaultPanel.getChart().setBackgroundPaint(null);

        chartDefaultPanel.getChart().getXYPlot().setRenderer(new PlotRenderer(this.tsDefaultPanel));
        chartDefaultPanel.getChart().getXYPlot().setBackgroundPaint(new GradientPaint(0, 0, Color.white, 0, 1000, Color.cyan));
        this.setPopupMenu(chartDefaultPanel);

        final ChartPanel chartOtherPanel = new ChartPanel(ChartFactory.createTimeSeriesChart("",
                                                                                             "Time",
                                                                                             "",
                                                                                             this.tsOtherPanel,
                                                                                             true,
                                                                                             true,
                                                                                             false));
        chartOtherPanel.getChart().setBackgroundPaint(null);
        chartOtherPanel.getChart().getXYPlot().setRenderer(new PlotRenderer(this.tsOtherPanel));
        chartOtherPanel.getChart().getXYPlot().setBackgroundPaint(new GradientPaint(0, 0, Color.white, 0, 1000, Color.cyan));
        this.setPopupMenu(chartOtherPanel);

        final Dimension panelDimension = new Dimension(650, 400);
        final Border checkBoxesPanelBorder = BorderFactory.createTitledBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(BevelBorder.LOWERED),
                                                                                                                 BorderFactory.createBevelBorder(BevelBorder.RAISED)),
                                                                              "IS Information",
                                                                              TitledBorder.LEADING,
                                                                              TitledBorder.TOP,
                                                                              new java.awt.Font(Font.DIALOG, Font.ITALIC, 12));

        final Border chartPanelBorder = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(0, 4, 4, 4),
                                                                           BorderFactory.createRaisedBevelBorder());

        final Border spinnerPanelBorder = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(0, 4, 4, 4),
                                                                             BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

        this.setLayout(new BorderLayout());
        final JTabbedPane tp = new JTabbedPane();

        final JPanel defPanel = new JPanel(new BorderLayout());
        {
            final JPanel spinnerPanel = new JPanel(new FlowLayout());
            spinnerPanel.add(new JLabel("Maximum period to plot:"));
            spinnerPanel.add(Box.createHorizontalStrut(3));
            spinnerPanel.add(this.daySpinnerDefaultPanel);
            this.daySpinnerDefaultPanel.addChangeListener(this.spinnerListenerDefaultPanel);
            spinnerPanel.add(new JLabel("Days"));
            spinnerPanel.add(this.hourSpinnerDefaultPanel);
            this.hourSpinnerDefaultPanel.addChangeListener(this.spinnerListenerDefaultPanel);
            spinnerPanel.add(new JLabel("Hours"));
            spinnerPanel.add(this.minutesSpinnerDefaultPanel);
            this.minutesSpinnerDefaultPanel.addChangeListener(this.spinnerListenerDefaultPanel);
            spinnerPanel.add(new JLabel("Minutes"));
            spinnerPanel.setBorder(spinnerPanelBorder);

            chartDefaultPanel.setPreferredSize(panelDimension);
            chartDefaultPanel.setBorder(chartPanelBorder);
            this.rbDefaultPanel.setBorder(checkBoxesPanelBorder);

            defPanel.add(this.rbDefaultPanel, BorderLayout.NORTH);
            defPanel.add(chartDefaultPanel, BorderLayout.CENTER);
            defPanel.add(spinnerPanel, BorderLayout.SOUTH);
        }

        final JPanel otherPanel = new JPanel(new BorderLayout());
        {
            final JPanel spinnerPanel = new JPanel(new FlowLayout());
            spinnerPanel.add(new JLabel("Maximum period to plot:"));
            spinnerPanel.add(Box.createHorizontalStrut(3));
            spinnerPanel.add(this.daySpinnerOtherPanel);
            this.daySpinnerOtherPanel.addChangeListener(this.spinnerListenerOtherPanel);
            spinnerPanel.add(new JLabel("Days"));
            spinnerPanel.add(this.hourSpinnerOtherPanel);
            this.hourSpinnerOtherPanel.addChangeListener(this.spinnerListenerOtherPanel);
            spinnerPanel.add(new JLabel("Hours"));
            spinnerPanel.add(this.minutesSpinnerOtherPanel);
            this.minutesSpinnerOtherPanel.addChangeListener(this.spinnerListenerOtherPanel);
            spinnerPanel.add(new JLabel("Minutes"));
            spinnerPanel.setBorder(spinnerPanelBorder);

            chartOtherPanel.setPreferredSize(panelDimension);
            chartOtherPanel.setBorder(chartPanelBorder);

            // The "other" panel may contain a great number of check boxes,
            // use a scroll panel to be able to show all of them
            final JPanel p = new JPanel(new BorderLayout());
            p.setBorder(checkBoxesPanelBorder);
            final SimpleScrollPane sp = new SimpleScrollPane(this.rbOtherPanel);
            sp.setHorizontalUnitIncrement(30);
            p.add(sp, BorderLayout.CENTER);

            otherPanel.add(p, BorderLayout.NORTH);
            otherPanel.add(chartOtherPanel, BorderLayout.CENTER);
            otherPanel.add(spinnerPanel, BorderLayout.SOUTH);
        }

        tp.add("Common Rates", defPanel);
        tp.add("Others", otherPanel);

        this.add(tp, BorderLayout.CENTER);
        this.add(this.toolBar, BorderLayout.SOUTH);
    }
}
