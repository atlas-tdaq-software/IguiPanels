package IguiPanels.DFPanel;

import Igui.IguiException;


abstract class DFPanelExceptions extends IguiException {
    private static final long serialVersionUID = -6591112391310264880L;

    public DFPanelExceptions(final String message) {
        super(message);
    }

    public DFPanelExceptions(final String message, final Throwable exCause) {
        super(message, exCause);
    }

    public DFPanelExceptions(final Throwable exCause) {
        super(exCause);
    }

    static class ISException extends DFPanelExceptions {
        private static final long serialVersionUID = 5352036658396779381L;

        ISException(final String message, final Throwable exCause) {
            super(message, exCause);
        }

        ISException(final String message) {
            super(message);
        }
    }

    static class ConfigException extends DFPanelExceptions {
        private static final long serialVersionUID = -4818915858928633220L;

        ConfigException(final String message, final Throwable exCause) {
            super(message, exCause);
        }

        ConfigException(final String message) {
            super(message);
        }
    }

    static class GenericException extends DFPanelExceptions {
        private static final long serialVersionUID = -4427897768525412665L;

        GenericException(final String message, final Throwable exCause) {
            super(message, exCause);
        }

        GenericException(final String message) {
            super(message);
        }
    }
}
